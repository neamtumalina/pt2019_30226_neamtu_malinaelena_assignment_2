package model;

import java.util.ArrayList;
import View.GUI1;

public class Store {

	private ArrayList<Queue> queue;
	GUI1 frame;
	int n;
	Boolean b;
	
	public Store(int n, GUI1 frame, Boolean bb) {
		this.b = bb;
		this.n= n;
		this.frame = frame;
		setQueue(new ArrayList<Queue>());
		info();
	}
	
	public void closeStore() {
		for(int i = 0; i < n; i++)
			getQueue().get(i).b = false;
	}
	
	public void info() {
		for(int i = 0; i < n; i++) {
			Queue q = new Queue(frame, b, i);
			getQueue().add(q);
		}
	}
	
	public void print() {
		for(int i = 0; i < n; i++) {
			System.out.println(i + ":" + getQueue().get(i));
		}
	}
	
	public int getFastestQueue() {
		int min = getQueue().get(0).getWaitTime();
		int id = 0;
		for(int i = 0; i < n; i++) {
			if(getQueue().get(i).getWaitTime() < min) {
				min = getQueue().get(i).getWaitTime();
				id = i;
			}
		}
		return id;
	}
	
	public void put(Client c) throws InterruptedException {
		int i = getFastestQueue();
		getQueue().get(i).push(c);
	}
	
	public int getSlowestQueue() {
		int max = 0;
		for(Queue x : getQueue()) {
			if(x.getWaitTime() > max) max = x.getWaitTime();
		}
		return max;
	}

	public ArrayList<Queue> getQueue() {
		return queue;
	}

	public void setQueue(ArrayList<Queue> queue) {
		this.queue = queue;
	}
}
