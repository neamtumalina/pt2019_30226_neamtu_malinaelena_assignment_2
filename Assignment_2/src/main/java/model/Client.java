package model;

public class Client {

	private int processingTime;
	private int arrivalTime;
	private static int number = 0;
	private int id;
	private int finishTime;
	
	public Client(int pTime, int arrival) {
		this.processingTime = pTime;
		this.arrivalTime = arrival;
		number += 1;
		id = number;
	}
		
	public String toString() {
		return " " + id + "(*-*) ";
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public int getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}
	
}
