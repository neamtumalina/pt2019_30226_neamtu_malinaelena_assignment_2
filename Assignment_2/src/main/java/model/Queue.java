package model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import View.GUI1;
import model.Client;

public class Queue extends Thread{

	BlockingQueue<Client> clients;
	private volatile AtomicInteger waitingTime = new AtomicInteger(0);
	private static final int SEC=1500;
	private int id;
	GUI1 frame;
	Boolean b;
	
	public Queue(GUI1 frame, Boolean b, int idd) {
		clients = new ArrayBlockingQueue<Client>(200);
		this.b = b;
		this.frame = frame;
		id = idd;
	}
	
	public AtomicInteger getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(AtomicInteger queueWaitingTime) {
		this.waitingTime = queueWaitingTime;
	}
	
	public void push(Client c) throws InterruptedException {
		c.setFinishTime(Integer.parseInt(GUI1.timeField.getText()) + c.getProcessingTime());
		clients.add(c);
		waitingTime.addAndGet(c.getProcessingTime());
		String str = "t(" + GUI1.timeField.getText() + ") |Q" + id + "|Entered client:" + c.getId() + " -> ProcessTime: " + c.getProcessingTime();
		GUI1.refresh(GUI1.simField, str);
	}
	
	
	public String toString() {
		String str = "";
		if (clients != null && clients.size() >= 1) {
			for (Client c : clients) {
				str += c.toString() + " ";
			}
		}
		return str;
			
	}
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getWaitTime() {
		return waitingTime.get();
	}

	public void run() {
		while(b) {
			while(clients.size() >= 1) {
				try {
					Client c;
					c = clients.element();
					sleep(c.getProcessingTime() * SEC);
					c = clients.take();
					int delay = Integer.parseInt(GUI1.timeField.getText()) - c.getFinishTime();
					String str = "t(" + GUI1.timeField.getText() + ") |Q" + id + "|Removed client:" + c.getId() + " -> Slowed with: " + delay;
					GUI1.refresh(GUI1.simField, str);
					waitingTime.addAndGet(-c.getProcessingTime());
					
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
	}
}
	
}
