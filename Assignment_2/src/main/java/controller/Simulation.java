package controller;

import java.awt.Color;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import View.GUI1;
import model.Client;
import model.Queue;
import model.Store;

public class Simulation extends Thread{

	private int simulationTime;
	private int nrOfQueues;
	private int minArrival;
	private int maxArrival;
	private int minProcessing;
	private int maxProcessing;
	int time = 0;
	private static final int SEC = 1500;
	Boolean b = true;
	Store myStore;
	GUI1 frame;

	
	public Simulation(int nr, int simTime, int minArr, int maxArr, int minProc, int maxProc, GUI1 frame) {
		this.simulationTime = simTime;
		this.minArrival = minArr;
		this.maxArrival = maxArr;
		this.minProcessing = minProc;
		this.maxProcessing = maxProc;
		this.frame = frame;
		nrOfQueues = nr;
		myStore = new Store(nrOfQueues, this.frame, b);
		myStore.print();
		for(int i = 0; i < nrOfQueues; i++) {
			myStore.getQueue().get(i).start();
		}
		
	}
	
	public void updateQueuesArea() {
		GUI1.queuesField.setText("");
		for(int i = 0; i < nrOfQueues; i++) {
			GUI1.queuesField.append("----------------------------------------------------------------------------------------------------\n");
			GUI1.queuesField.append("|" + i + "| " + myStore.getQueue().get(i).toString()+"\n");
			GUI1.queuesField.append("Queue's time: " +  myStore.getQueue().get(i).getWaitingTime() + "\n");
			GUI1.queuesField.append("----------------------------------------------------------------------------------------------------\n");
		}
	}
	
	public void run() {
		while(time < simulationTime) {
			try {
				Random rd = new Random();
				int process = rd.nextInt(maxProcessing - minProcessing + 1) + minProcessing;
				int arrival = rd.nextInt(maxArrival - minArrival + 1) + minArrival;
				Client c = new Client(process, arrival);
				if(c.getProcessingTime() + time >= simulationTime){
					GUI1.simField.append("t(" + time + ") We are closing, you need to hurry client" + c.getId() + "\n");
					time += 1;
					frame.timeField.setText(time + "");
					sleep(SEC);
					continue;
				}
				int i = 0;
				while(i < c.getArrivalTime()) {
					updateQueuesArea();
					time += 1;
					frame.timeField.setText(time+"");
					sleep(SEC);
					i++;
				}
				myStore.put(c);
				myStore.print();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
		GUI1.simField.append(time + " | Store closed\n");
		//System.out.println(myStore.getQueue().get(0).toString());
		updateQueuesArea();
		myStore.closeStore();
	}
		
	public int getNrOfQueues() {
		return nrOfQueues;
	}

	public void setNrOfQueues(int nrOfQueues) {
		this.nrOfQueues = nrOfQueues;
	}

	public int getMinArrival() {
		return minArrival;
	}

	public void setMinArrival(int minArrival) {
		this.minArrival = minArrival;
	}

	public int getMaxArrival() {
		return maxArrival;
	}

	public void setMaxArrival(int maxArrival) {
		this.maxArrival = maxArrival;
	}

	public int getMinProcessing() {
		return minProcessing;
	}

	public void setMinProcessing(int minProcessing) {
		this.minProcessing = minProcessing;
	}

	public int getMaxProcessing() {
		return maxProcessing;
	}

	public void setMaxProcessing(int maxProcessing) {
		this.maxProcessing = maxProcessing;
	}	
	
}