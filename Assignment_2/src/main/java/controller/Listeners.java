package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import View.GUI;
import View.GUI1;
import controller.Simulation;


public  class Listeners implements ActionListener{

	private Simulation sim;
	private int simInterval = 23;
	private int minArrival = 1;
	private int maxArrival = 4;
	private int minProcessing = 1;
	private int maxProcessing = 6;
	
	public void actionPerformed(ActionEvent e) {
		 if(e.getSource() == GUI.start) {
			GUI1 myView = new GUI1();
			
			int nrQueues = Integer.parseInt(GUI.nr.getText());
			simInterval =Integer.parseInt(GUI.sim.getText());
			minArrival = Integer.parseInt(GUI.mi.getText());
			maxArrival = Integer.parseInt(GUI.ma.getText());
			minProcessing = Integer.parseInt(GUI.mip.getText());
			maxProcessing = Integer.parseInt(GUI.map.getText());
			
			myView.setVisible(true);
			sim = new Simulation(nrQueues, simInterval, minArrival, maxArrival, minProcessing, maxProcessing, myView);
			sim.start();
			 }
	}

}