package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI1 extends JFrame {

	private static final long serialVersionUID = 1L;
	public JTextField nr = new JTextField(5);
	public static JTextArea simField = new JTextArea(100, 50);
	public static JTextField timeField = new JTextField(3);
	public JLabel hour = new JLabel(" HOUR: ");
	Font myFont = new Font("Arial", Font.BOLD, 20);
	int n = Integer.parseInt(GUI.nr.getText());
	private JLabel informatii = new JLabel("Simulation: ");
	public static JTextArea queuesField = new JTextArea(100, 50);

	public static JPanel frame = new JPanel();

	public GUI1() {

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1800, 600);
		setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - getSize().width) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2);

		frame.setBackground(Color.black);
		frame.setPreferredSize(new Dimension(1800, 600));
		frame.setLayout(new BoxLayout(frame, BoxLayout.X_AXIS));

		JPanel f1 = new JPanel();
		f1.setPreferredSize(new Dimension(900, 600));
		f1.setBackground(Color.gray);
		queuesField.setText("");
		queuesField.setFont(myFont);
		queuesField.setBackground(Color.black);
		queuesField.setForeground(Color.WHITE);
		f1.add(queuesField);
		f1.add(Box.createRigidArea(new Dimension(20, 20)));
		frame.add(f1);
		
		frame.add(Box.createRigidArea(new Dimension(20, 20)));

		JPanel f2 = new JPanel();
		f2.setPreferredSize(new Dimension(900, 600));
		f2.setLayout(new BoxLayout(f2, BoxLayout.Y_AXIS));
		f2.setBackground(Color.black);
		JScrollPane sp = new JScrollPane(simField);
		simField.setText("");
		simField.setFont(myFont);
		simField.setBackground(Color.LIGHT_GRAY);
		sp.setPreferredSize(new Dimension(900, 500));
		sp.setVisible(true);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		informatii.setFont(myFont);
		informatii.setForeground(Color.green);
		f2.add(informatii);
		f2.add(sp);
		
		f2.add(Box.createRigidArea(new Dimension(20, 20)));
		
		hour.setFont(myFont);
		hour.setForeground(Color.WHITE);
		timeField.setFont(myFont);
		timeField.setBackground(Color.black);
		timeField.setForeground(Color.WHITE);
		JPanel f3 = new JPanel();
		f3.setLayout(new BoxLayout(f3, BoxLayout.X_AXIS));
		f3.setBackground(Color.black);
		f3.add(hour);
		f3.add(timeField);
		f2.add(f3);
		
		frame.add(f2);
		frame.add(Box.createRigidArea(new Dimension(20, 20)));


		this.setContentPane(frame);

	}

	public static JTextArea refresh(JTextArea ta, String str) {
		JTextArea textArea = ta;
		textArea.setText(textArea.getText() + str + "\n");
		return ta;
	}
}
