package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controller.Listeners;

public class GUI extends JFrame{


	private static final long serialVersionUID = 1L;
	public static JPanel frame = new JPanel();
	private JLabel  arrivigTime = new JLabel ("Arriving time between customers: ");
	private JLabel  min = new JLabel ("min: ");
	private JLabel  max = new JLabel ("max: ");
	private JLabel  minp = new JLabel ("min: ");
	private JLabel  maxp = new JLabel ("max: ");
	private JLabel  processingTime = new JLabel ("Processing Time: ");
	private JLabel  nrQueues = new JLabel ("Number of Queues: ");
	private JLabel  simInterval = new JLabel ("Simulation Interval: ");
	public static  JButton start = new JButton(" Start ");
	public static JTextField mi = new JTextField(10);
	public static JTextField ma = new JTextField(10);
	public static JTextField mip = new JTextField(10);
	public static JTextField map = new JTextField(10);
	public static JTextField nr = new JTextField(10);
	public static JTextField sim = new JTextField(10);
	
	public GUI(){
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(700, 500);
		setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  - getSize().width) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2);
	
		frame.setLayout(new BoxLayout(frame,BoxLayout.Y_AXIS));
		frame.setBackground(Color.black);
		
		
		frame.add(Box.createRigidArea(new Dimension(20,20)));
		
		arrivigTime.setForeground(Color.white);
		arrivigTime.setFont(new Font("Arial", Font.BOLD, 20));
		frame.add(arrivigTime);
		frame.add(Box.createRigidArea(new Dimension(10,10)));
		
		JPanel panel1 = new JPanel();
		panel1.setBackground(Color.black);
		min.setForeground(Color.white);
		min.setFont(new Font("Arial", Font.BOLD, 20));
		mi.setFont(new Font("Arial", Font.BOLD, 20));
		max.setForeground(Color.white);
		max.setFont(new Font("Arial", Font.BOLD, 20));
		ma.setFont(new Font("Arial", Font.BOLD, 20));
		panel1.add(min);
		panel1.add(mi);
		panel1.add(Box.createRigidArea(new Dimension(15,15)));
		panel1.add(max);
		panel1.add(ma);
		frame.add(panel1);
		
		processingTime.setForeground(Color.white);
		processingTime.setFont(new Font("Arial", Font.BOLD, 20));
		frame.add(processingTime);
		frame.add(Box.createRigidArea(new Dimension(10,10)));
		JPanel panel4 = new JPanel();
		panel4.setBackground(Color.black);
		minp.setForeground(Color.white);
		minp.setFont(new Font("Arial", Font.BOLD, 20));
		mip.setFont(new Font("Arial", Font.BOLD, 20));
		maxp.setForeground(Color.white);
		maxp.setFont(new Font("Arial", Font.BOLD, 20));
		map.setFont(new Font("Arial", Font.BOLD, 20));
		panel4.add(minp);
		panel4.add(mip);
		panel4.add(Box.createRigidArea(new Dimension(15,15)));
		panel4.add(maxp);
		panel4.add(map);
		frame.add(panel4);
		
		JPanel panel2 = new JPanel();
		panel2.setBackground(Color.black);
		nrQueues.setForeground(Color.white);
		nrQueues.setFont(new Font("Arial", Font.BOLD, 20));
		nr.setFont(new Font("Arial", Font.BOLD, 20));
		panel2.add(nrQueues);
		panel2.add(nr);
		frame.add(panel2);
		
		JPanel panel3 = new JPanel();
		panel3.setBackground(Color.black);
		simInterval.setForeground(Color.white);
		simInterval.setFont(new Font("Arial", Font.BOLD, 20));
		sim.setFont(new Font("Arial", Font.BOLD, 20));
		panel3.add(simInterval);
		panel3.add(sim);
		frame.add(panel3);
		
		start.setFont(new Font("Arial", Font.BOLD, 20));
		start.setForeground(Color.green);
		start.setPreferredSize(new Dimension(60, 40));
		frame.add(start);
		frame.add(Box.createRigidArea(new Dimension(20,20)));
		
		this.setContentPane(frame);
		
		Listeners myHandler = new Listeners();
		start.addActionListener(myHandler);
	}
}
